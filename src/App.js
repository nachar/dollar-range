import React from 'react';
import TestComponent from './components/TestComponent';

function App() {
  return (
    <div className="App">
      <h1 className="text-center">Test</h1>
      <TestComponent />
    </div>
  );
}

export default App;
