import React, { useState } from 'react';

const DollarRange = () => {
  const [activeIndex, setActiveIndex] = useState(1);
  const changeIndex = () => {
    setActiveIndex(activeIndex + 1);
  };

  return (
    <div>
      <h1 className="text-center">
        Active index:
        { activeIndex }
      </h1>
      <button onClick={() => changeIndex()} type="button" className="btn btn-primary mx-auto d-block">change</button>
    </div>
  );
};

export default DollarRange;
